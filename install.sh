#!/bin/sh

for i in .bashrc .emacs/.emacs .emacs/.emacs.d .fonts .gdbinit .gitconfig .pystartup .screenrc .shellrc .Xresources .zshrc .tmux.conf
do
    ln -sf $PWD/$i ~/
done

rm ~/.ssh/authorized_keys
ln -sf $PWD/authorized_keys ~/.ssh/authorized_keys
